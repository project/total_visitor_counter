About
=====
This is a simple block that shows a visitor counter with humans, robots and any visitor to the webpage.
This module has a configuration to set the start value.

Installation
============
composer require drupal/total_visitor_counter:^dev

Usage
======
Configure the conter start at /admin/config/system/total-visitor-counter
Set the block total_visitor_counter into the region that you need to use as usual.
