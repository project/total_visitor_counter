<?php

namespace Drupal\total_visitor_counter\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "total_visitor_counter_block",
 *   admin_label = @Translation("Total visitor counter"),
 *   category = @Translation("Total visitor counter")
 * )
 */
class TotalVisitorCounterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $current = \Drupal::config('total_visitor_counter.settings')->get('init_counter');
    $current++;
    $build['content'] = [
      '#markup' => $this->t(':q', [':q' => $current]),
    ];
    $config = \Drupal::service('config.factory')->getEditable('total_visitor_counter.settings');
    $config->set('init_counter', $current);
    $config->save();

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
