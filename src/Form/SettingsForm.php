<?php

namespace Drupal\total_visitor_counter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Total visitor counter settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'total_visitor_counter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['total_visitor_counter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['init_counter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Init Counter'),
      '#default_value' => $this->config('total_visitor_counter.settings')->get('init_counter'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('init_counter') < 0 ) {
      $form_state->setErrorByName('init_counter', $this->t('The value is not correct.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('total_visitor_counter.settings')
      ->set('init_counter', $form_state->getValue('init_counter'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
